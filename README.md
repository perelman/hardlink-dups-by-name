# hardlink-dups-by-name

Searches two directory trees in parallel and hardlinks identical files,
only comparing files with identical relative paths.

```
USAGE: hardlink-dups-by-name.sh [--clobber=none|first|second] [--verbose] [--] search_dir other_dir
```
